<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


Route::get('/',['uses' => 'WebController@index'  , 'as' => 'Web.Login'] );

Route::post('/file-upload', 'FileController@upload');

Route::get('/cadmin',['uses' => 'DashboardController@index'  , 'as' => 'Backend.Dashboard.Index'] );

Route::get('/cadmin/land',['uses' => 'LandController@index'  , 'as' => 'Backend.Land.Index'] );
Route::get('/cadmin/land/create',['uses' => 'LandController@create'  , 'as' => 'Backend.Land.Create'] );
Route::get('/cadmin/land/{id}',['uses' => 'LandController@edit'  , 'as' => 'Backend.Land.Edit'] );
Route::get('/cadmin/land/{id}/view',['uses' => 'LandController@view'  , 'as' => 'Backend.Land.View'] );
Route::get('/cadmin/land/{id}/photo',['uses' => 'LandController@photo'  , 'as' => 'Backend.Land.Photo'] );


Route::get('/cadmin/question',['uses' => 'QuestionController@index'  , 'as' => 'Backend.Question.Index'] );
Route::get('/cadmin/question/create',['uses' => 'QuestionController@create'  , 'as' => 'Backend.Question.Create'] );
Route::get('/cadmin/question/{id}',['uses' => 'QuestionController@edit'  , 'as' => 'Backend.Question.Edit'] );
Route::get('/cadmin/question/{id}/view',['uses' => 'QuestionController@view'  , 'as' => 'Backend.Question.View'] );

Route::get('/cadmin/user',['uses' => 'UserController@index'  , 'as' => 'Backend.User.Index'] );
Route::get('/cadmin/user/create',['uses' => 'UserController@create'  , 'as' => 'Backend.User.Create'] );
Route::get('/cadmin/user/{id}',['uses' => 'UserController@edit'  , 'as' => 'Backend.User.Edit'] );


Route::get('/cadmin/rice',['uses' => 'RiceController@index'  , 'as' => 'Backend.Rice.Index'] );
Route::get('/cadmin/rice/create',['uses' => 'RiceController@create'  , 'as' => 'Backend.Rice.Create'] );
Route::get('/cadmin/rice/{id}',['uses' => 'RiceController@edit'  , 'as' => 'Backend.Rice.Edit'] );


Route::get('/cadmin/fertilizer',['uses' => 'FertilizerController@index'  , 'as' => 'Backend.Fertilizer.Index'] );
Route::get('/cadmin/fertilizer/create',['uses' => 'FertilizerController@create'  , 'as' => 'Backend.Fertilizer.Create'] );
Route::get('/cadmin/fertilizer/{id}',['uses' => 'FertilizerController@edit'  , 'as' => 'Backend.Fertilizer.Edit'] );