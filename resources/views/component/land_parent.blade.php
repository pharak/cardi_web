<script>
    function component_land_parent(){
           $.ajax({
               type: 'Get',
               url:  "{{ env('API_URL') }}land_parent",
               headers: { 'Authorization': "bearer " +  window.localStorage.getItem('refresh_token'),
                        'Access_Token': window.localStorage.getItem('access_token')
                },
               success: function(resultData) { 
                   $.each(resultData, function(key, value) {
                       $("#parent_id").append(
                           $('<option></option>').val(key).html(value)
                       );
                   });
               }
           })
       }
       
       component_land_parent();
</script>