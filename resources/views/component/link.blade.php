<script>
    function component_land_parent(){
           $.ajax({
               type: 'Get',
               url:  "{{ env('API_URL') }}link",
               headers: { 'Authorization': "bearer " +  window.localStorage.getItem('refresh_token'),
                        'Access_Token': window.localStorage.getItem('access_token')
                },
               success: function(resultData) { 
                   $.each(resultData, function(key, value) {
                       $("select[name=link]").append(
                           $('<option></option>').val(key).html(value)
                       );
                   });
               }
           })
       }
       
       component_land_parent();
</script>