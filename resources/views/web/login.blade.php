@extends('layouts.cooladmin.login.master')
@section('Content')
    <div class="login-logo">
        <a href="#">
            <img src="{{ url('CARDI_logo.JPG') }}" alt="CoolAdmin" width="320">
        </a>
    </div>
    <div class="login-form">
        <form action=""  id='loginform'  action-api-post='{{ env('API_URL').'login' }}' >
            <div class="form-group">
                <label>Username</label>
                <input class="au-input au-input--full" type="username" name="username" placeholder="Username">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
            </div>
            <button class="au-btn au-btn--block au-btn--green m-b-20">sign in</button>
        </form>
    </div>
@endsection

@push('JS')
    <script>
        $(document).ready(function(e){
            $("#loginform").submit(function(e){
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url:  $('#loginform').attr("action-api-post"),
                    data: $('#loginform').serialize(),
                    success: function(resultData) { 
                        if (typeof(Storage) !== "undefined") {
                            // Store
                            sessionStorage.setItem("token",  resultData.token.original.token);
                            sessionStorage.setItem("token_type",  resultData.token.original.token_type);
                            var dExpire = new Date();
                            dExpire = new Date(dExpire.getTime() + (1000 * resultData.token.original.expires_in));
                            sessionStorage.setItem("expired_at",  dExpire);
                            sessionStorage.setItem("username",  resultData.user.username);
                            sessionStorage.setItem("fullname",  resultData.user.fullname);
                            sessionStorage.setItem("email",  resultData.user.email);
                            sessionStorage.setItem("role", resultData.user.role.title);                            
                            $(".waiting").hide();
                            window.location.href = "cadmin";
                        // Code for localStorage/sessionStorage.
                        } else {
                            alert("You Can't login");
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                        alert("User & Password can't login");
                        $(".waiting").hide();
                    }  
			    })
    
            });
        });

    </script>
@endpush