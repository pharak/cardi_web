@extends('layouts.cooladmin.backend.master')
@push('CSS')
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
    <style>.text-right{text-align: right}</style>
@endpush
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row">
                    <div class="col-md-6">
                        <h3 class="title-5 m-b-35">អ្នកប្រើប្រាស់</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('Backend.User.Create') }}" class="btn btn-primary">បង្កើត</a>
                    </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <table id="mytable" class="table table-data2">
                    <thead>
                        <tr>
                            <th>N<sup>0</sup></th>
                            <th>ឈ្មោះ User</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('JS')
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            var table = $('#mytable').DataTable( {
                "processing": false,
                "serverSide": false,
                "ajax":{
                    url: "{{ env('API_URL') }}user",
                    type: "GET"
                },
                "columns" : [
                    
                    {'data':'id'},
                    {'data':'fullname'},
                    {
                        sortable: false,
                        "render": function ( data, type, full, meta ) {
                            var buttonID = full.id;
                            var $str = "";
                            $str += '<a id='+buttonID+' class="btn btn-primary btn-edit" role="button" href="{{ url('cadmin/user') }}/'+ buttonID +'">កែរ</a>';
                            return $str;
                        }
                    },
                 
                ]
            })
        } );
    </script>
@endpush