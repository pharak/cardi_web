@extends('layouts.cooladmin.backend.master')
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title-5 m-b-35">អ្នកប្រើប្រាស់ (បង្កើត)</h3>
                </div>
                <div class="col-md-6 text-fight">

                </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <form id="myform" class="myform" action-api-post="{{ env('API_URL').'user' }}"  action-main-url="{{ url('cadmin/user') }}">
                    @include("backend.user.partials.form")
                    <div class="row">
                        <div class="col-sm-6 right">
                            <button type="submit" class="btn btn-success" id="btn-post">យល់ព្រម</button>
                        </div>
                        <div class="col-sm-6 left">
                            <input type="button" class="btn btn-primary" id="btn-cancel" value="បដិសេធ">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('JS')
    @include('macro.post')
@endpush