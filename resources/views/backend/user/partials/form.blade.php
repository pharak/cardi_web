<div class="form-group">
    <label>គោត្តនាម និងនាម</label>
    <input class="au-input au-input--full" type="text"  data-bind-fullname name="CRUD[fullname]" placeholder="គោត្តនាម និងនាម" required>
</div>

<div class="form-group">
    <label>អាសយដ្ឋានអេឡិចត្រូនិច</label>
    <input class="au-input au-input--full" type="email"  data-bind-email name="CRUD[email]" placeholder="Email" required>
</div>

<div class="form-group">
    <label>អាសយដ្ឋានអេឡិចត្រូនិច</label>
    <input class="au-input au-input--full" type="text"  data-bind-tel name="CRUD[tel]" placeholder="Tel" required>
</div>
<div class="form-group">
    <label>គណនី</label>
    <input class="au-input au-input--full" type="text"  data-bind-username name="CRUD[username]" placeholder="Username" required @if(@$id) {{ 'Disabled' }} @endif>
</div>
<div class="form-group">
    <label>លេខសម្ងាត់</label>
    <input class="au-input au-input--full" type="text"  name="CRUD[password]" placeholder="Password"  @if(!@$id) required @endif>
</div>
<div class="form-group">
    <label>សិទ្ធ</label>
    <select class="au-input au-input--full" name="CRUD[role_id]" id="role_id" data-bind-role_id >
        <option value='1'>Admin</option>
        <option value='2'>Editor</option>
    </select>
</div>