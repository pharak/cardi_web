@extends('layouts.cooladmin.backend.master')
@push('CSS')
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
@endpush
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row">
                    <div class="col-md-6">
                        <h3 class="title-5 m-b-35">ប្រភេទស្រូវ</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('Backend.Fertilizer.Create') }}" class="btn btn-primary">Create</a>
                    </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <table id="mytable" class="table table-data2">
                    <thead>
                        <tr>
                            <th>N<sup>0</sup></th>
                            <th>ឈ្មោះប្រភេទស្រូវ</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('JS')
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $.fn.dataTable.ext.errMode = 'none';
            var table = $('#mytable').DataTable( {
                "processing": false,
                "serverSide": false,
                "ajax":{
                    url: "{{ env('API_URL') }}rice",
                    type: "GET"
                },
                "columns" : [
                    
                    {'data':'id'},
                    {'data':'title_kh'},
                    {
                        sortable: false,
                        "render": function ( data, type, full, meta ) {
                            var buttonID = full.id;
                            var $str = "";
                            $str += '<a id='+buttonID+' class="btn btn-primary btn-edit" role="button" href="{{ url('cadmin/rice') }}/'+ buttonID +'">កែរ</a>';
                            return $str;
                        }
                    }

                ]
            })
        } );
    </script>
@endpush