@extends('layouts.cooladmin.backend.master')
@push('CSS')
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
@endpush
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row">
                    <div class="col-md-6">
                        <h3 class="title-5 m-b-35">ក្រុមដី (កំណែរ)</h3>
                    </div>
                    <div class="col-md-6 text-fight">

                    </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <form id="myform" class="myform" action-api-post="{{ env('API_URL').'fertilizer' }}/{{ $id }}"  action-main-url="{{ url('cadmin/fertilizer') }}">
                    <input type="hidden" name="CRUD[id]" value="{{ $id }}" />
                    <input type="hidden" name="_method" value="PUT" />
                    @include("backend.fertilizer.partials.form")
                    <div class="row">
                        <div class="col-sm-6 right">
                            <button type="submit" class="btn btn-success" id="btn-post">យល់ព្រម</button>
                        </div>
                        <div class="col-sm-6 left">
                            <input type="button" class="btn btn-primary" id="btn-cancel" value="បដិសេធ">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('JS')
    @include('macro.retreive',['moduler'=>'fertilizer','id'=>$id, 'EDITOR'=>true])
    @include('macro.post_upload')
@endpush