@extends('layouts.cooladmin.backend.master')
@push('CSS')
    @include("backend.question.partials.structure_css")
@endpush
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">សំណួរ</h3>
                </div>
                <div class="col-sm-12">
                    <div class="body genealogy-body genealogy-scroll">
                        <div class="genealogy-tree" id="DivIdToPrint">
                            @include("backend.question.partials.structure_css")
                            
                        </div>
                    </div>
                    <input type='button' class="btn btn-success" id='btn' value='Print' onclick='printDiv();'>
                </div>
                
            </div>
        </div>
    </div>
@endsection

@push('JS')
    @include("backend.question.partials.structure_js")
    @include("popup.answer")
    @include("popup.text_answer")
    @include("component.link")
    <script>
        function printDiv() 
        {

        var divToPrint=document.getElementById('DivIdToPrint');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

        }
            
        $('.ddlAnswer').change(function(){
            if($(this).val() != '1'){
                $('.answer-block').hide();
            }else{
                $('.answer-block').show();
            }
        });
        function generateQuestion($items){
            let $str = "";
            $($items).each(function(index, item){
                $str += "<li>";
                    $str += '<div class="member-view-box">';
                        if(item.answer_type_id == 1){
                            $str += '<div class="member-details" data-bind-id='+item.id+'>';
                        }else if(item.answer_type_id == 2){
                            $str += '<div class="member-details_button_yes">';
                        }else if(item.answer_type_id == 3){
                            $str += '<div class="member-details_button_no">';
                        }
                        /*else if(item.answer_type_id == 4){
                            $str += '<div class="member-details_button_link">';
                        }*/
                            $str += "<b>"+item.id+"</b>"+" . ";
                            
                            if(item.link){
                                $str += "<u>"+ "<span class='btn-edit'  data-bind-id='"+item.id+"'>"+item.title_kh+"</span>"+"</u> ";

                                $str += "<i class='setup btn-view' data-bind-id="+item.id+">V</i>";
                                
                            }else{
                                $str += "<span class='btn-edit' data-bind-id='"+item.id+"'>"+item.title_kh+"</span>";
                            }

                            if(item.parent_id !== null){
                                $str += "<span class='setup btn-remove' data-bind-id="+item.id+">-</span>";
                            }
                               $str += "<span class='setup btn-add' data-bind-id="+item.id+">+</span>";
                            
                        $str += '</div>';
                    $str += '</div>';
                    if(item.answers && item.answers.length > 0){
                        $str += "<ul class='active'>";
                            $str += generateQuestion(item.answers); 
                        $str += "</ul>";
                    }
                $str += "</li>";
            })
            return $str;
        }
        function showQuestion(){
            $.ajax({
                type: 'GET',
                url:  "{{ env('API_URL').'question/'.$id }}",
                success: function(resultData) {
                    var $displayStr = '';
                    $displayStr += "<ul>";
                    $displayStr += generateQuestion(resultData);
                    $displayStr += "</ul>";
                    $('.genealogy-tree').html($displayStr);
                }
            });
        }
        showQuestion();
      
        $(document).on('click','.btn-add', function(){
            form_clear();
            $('#dialog-answer').modal('show');
            var i = $(this).attr("data-bind-id");
            $('#parent_id').val(i);
            $('.answer-block').show();
            $('#type').val('add');
        });
        
        $(document).on('click','.btn-edit', function(){
            form_clear();
            $id = $(this).attr('data-bind-id');
            $.ajax({
                type: 'get',
                url:  "{{ env('API_URL').'question/' }}" + $id,
                headers: {
                            'Authorization': "bearer " +  window.sessionStorage.getItem('refresh_token'),
                            'Access_Token': window.sessionStorage.getItem('access_token')
                        },
                success: function(resultData) {             
                    $('#dialog-text_answer').modal('show');
                    var i = $(this).attr("data-bind-id");
                    $("#title_kh_text").val(resultData.title_kh);
                    $("#id_text").val(resultData.id);
                    $('#ddl-land-text').val(resultData.link);
                },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    alert("Problem");
                    //$(".waiting").hide();
                }
            });


        });


        

        function form_clear(){
            $('.ddlAnswer').val(1);
            $('#parent_id').val("");
            $('.ddlAnswer').val();
            $('#title_kh').val('');
            $('#ddl-land').val(0);
            $('#type').val('');
        }
        $(document).on('click','.btn-remove', function(){
            if(confirm('តើអ្នកចង់លុបចេញ?')){
                var getId = $(this).attr('data-bind-id');
                var getUrl = "{{ env('API_URL').'question-updatesub' }}/"+ getId +"/delete";
                $.ajax({
                    type: 'DELETE',
                    url: getUrl,
                    headers: {
                                'Authorization': "bearer " +  window.sessionStorage.getItem('refresh_token'),
                                'Access_Token': window.sessionStorage.getItem('access_token')
                            },
                    success: function(resultData) { 
                        //$(".waiting").hide();
                        window.location.reload();
                    },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Problem");
                        //$(".waiting").hide();
                    }
                });
            }
        });
    </script>
@endpush