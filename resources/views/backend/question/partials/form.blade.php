<div class="form-group">
    <label>ឈ្មោះក្រុមដីជាភាសាខ្មែរ</label>
    <input class="au-input au-input--full" type="text" name="CRUD[title_kh]" data-bind-title_kh id="title_kh" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" required>
</div>
<div class="form-group">
    <label>ឈ្មោះក្រុមដីជាអង់គ្លេស</label>
    <input class="au-input au-input--full" type="text" name="CRUD[title_en]" data-bind-title_en id="title_en" placeholder="ឈ្មោះក្រុមដីជាអង់គ្លេស" required>
</div>