
<div class="row">
    <div class="col-sm-8">

        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">ភាសាខ្មែរ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">ភាសាអង់គ្លេស</a>
                    </li>
                </ul>
                <div class="tab-content pl-3 p-1" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="form-group">
                            <label>ឈ្មៅះដំណាំជាភាសាខ្មែរ</label>
                            <input class="au-input au-input--full" type="text" name="CRUD[title_kh]" data-bind-title_kh id="title_kh" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" required>
                        </div>
                        <div class="form-group">
                            <label>បរិយាយជាភាសាខ្មែរ</label>
                            <textarea class="au-input au-input--full" name="CRUD[description_kh]" data-bind-description_kh id="description_kh" data-form-textarea="true" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" ></textarea>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="form-group">
                            <label>ឈ្មៅះដំណាំជាភាសាអង់គ្លេស</label>
                            <input class="au-input au-input--full" type="text" name="CRUD[title_en]" id="title_en"  data-bind-title_kh id="title_en" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" required>
                        </div>
                        <div class="form-group">
                            <label>បរិយាយជាភាសាអង់គ្លេស</label>
                            <textarea class="au-input au-input--full" name="CRUD[description_en]" id="description_en" data-bind-title_kh data-form-textarea="true" id="description_en"  placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" ></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </div>
    
    <div class="col-sm-4 center">
        <div class="form-group">
            <label>រូបតំណាង 414x220</label>
            <label>
                <img id="img_thumbnail" src="https://theme.zdassets.com/theme_assets/22351/0a5c59c344538bf6ed384ba542670d4443357575.png" alt="your image" class="img-responsive">
                <input type="file" class="au-input " style="display: none" name="CRUD[thumbnail]" id="thumbnail"  data-bind-to="img_thumbnail" data-bind-thumbnail  data-form-image="true">
            </label>
        </div>
    </div>


    <div class="col-sm-4 center">
        <div class="form-group">
            <label>ឯកសារ</label>
            <label>
                <input type="file" class="au-input "  name="CRUD[file]" id="file"  data-bind-to="file" data-bind-file>
            </label>
        </div>
    </div>

    <div class="col-sm-4 center">
        <div class="form-group">
            <label>Link Download</label>
            <label>
                <input type="text" class="au-input "  name="CRUD[link]" id="download"  data-bind-to="link" data-bind-link>
            </label>
        </div>
    </div>

</div>

@push('JS')
    <script>
        window.addEventListener('load', function() {
            document.getElementById('thumbnail').addEventListener('change', function() {
                if (this.files && this.files[0]) {
                    var img = document.getElementById('img_thumbnail');  // $('img')[0]
                    img.src = URL.createObjectURL(this.files[0]); // set src to blob url
                }
            });
        });
        tinymce.init({selector: '#description_kh', statusbar: false, setup: function (editor) { editor.on('change', function () {tinymce.triggerSave(); }); }});
        tinymce.init({selector: '#description_en', statusbar: false, setup: function (editor) { editor.on('change', function () {tinymce.triggerSave(); }); }});

        

    </script>
@endpush