@extends('layouts.cooladmin.backend.master')
@push('CSS')
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
    <style>
        .card-body{text-align: center;}
        .img-land{max-height: 160px;}
    </style>
@endpush
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row" style="margin-bottom:20px;">
                <div class="col-md-6">
                    <h3 class="title-5 m-b-35">រូបថត</h3>
                    <form id="formz" action-api-post="{{ env('API_URL') }}land/{{$id}}/photo">
                        <input type="file" name="url" id="url">
                        <input type="button"  class="btn btn-success" id="btn-upload" value="Upload"  accept="image/*">
                    </form>
                    <hr/>
                </div>
            </div>

            <div class="row content-image">

            </div>
        </div>
    </div>
@endsection

@push('JS')

    <script>
         $.ajax({
               type: 'Get',
               url:  "{{ env('API_URL') }}land/{{$id}}/photo",
               headers: { 'Authorization': "bearer " +  window.localStorage.getItem('refresh_token'),
                        'Access_Token': window.localStorage.getItem('access_token')
                },
               success: function(resultData) { 
                   var $str = "";
                   $.each(resultData, function(key, value) {
                    $str += '<div class="col-sm-3">';
                        $str += '<div class="card">';
                            $str += '<div class="card-header"></div>';
                            $str += '<div class="card-body">';
                                $str += '<img src="'+value.url+'" class="img-responsive img-land">';
                            $str +='</div>';
                            $str += '<div class="card-footer">';
                                $str += '<input type="button" data-bind-id="'+value.id+'" class="btn btn-danger btn-delete">';
                            $str +='</div>';
                        $str += '</div>';
                    $str += '</div>';
                    $('.content-image').append($str);
                   });

               }
           });
           $("#btn-upload").click(function(){
        var token = window.sessionStorage.getItem('access_token');
        var getUrl2 =   $('#formz').attr("action-api-post");
        var getData = new FormData($("#formz")[0]);
                $.ajax({
                    type: 'POST',
                    url:  $('#formz').attr("action-api-post"),
                    data: getData,
                    dataType: "html",
                    contentType: false,
                    processData: false,
                    headers: {
                                'Authorization': "bearer " +  window.sessionStorage.getItem('refresh_token'),
                                'Access_Token': window.sessionStorage.getItem('access_token')
                            },
                    success: function(resultData) { 
                        $(".waiting").hide();
                        window.location.reload();
                    },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Problem");
                        $(".waiting").hide();
                    }
                });
           });

           $(document).on("click",".btn-delete",function(){
                if(confirm('តើអ្នកចង់លុបចេញ?')){
                    var getId = $(this).attr('data-bind-id');
                    var getUrl = "{{ env('API_URL').'land/'.$id }}/photo/"+getId;
                    $.ajax({
                        type: 'DELETE',
                        url: getUrl,
                        headers: {
                                    'Authorization': "bearer " +  window.sessionStorage.getItem('refresh_token'),
                                    'Access_Token': window.sessionStorage.getItem('access_token')
                                },
                        success: function(resultData) { 
                            //$(".waiting").hide();
                            window.location.reload();
                        },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                            alert("Problem");
                            //$(".waiting").hide();
                        }
                    });
                }
           });
    </script>


@endpush