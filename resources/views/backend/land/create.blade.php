@extends('layouts.cooladmin.backend.master')
@push('CSS')
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
@endpush
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row">
                    <div class="col-md-6">
                        <h3 class="title-5 m-b-35">ក្រុមដី (បង្កើត)</h3>
                    </div>
                    <div class="col-md-6 text-fight">

                    </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <form id="myform" class="myform" action-api-post="{{ env('API_URL').'land' }}"  action-main-url="{{ url('cadmin/land') }}">
                    @include("backend.land.partials.form")
                    <div class="row">
                        <div class="col-sm-6 right">
                            <button type="submit" class="btn btn-success" id="btn-post">យល់ព្រម</button>
                        </div>
                        <div class="col-sm-6 left">
                            <input type="button" class="btn btn-primary" id="btn-cancel" value="បដិសេធ">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('JS')
    @include('macro.post_upload')
@endpush