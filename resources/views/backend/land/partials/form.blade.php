
<div class="row">
    <div class="col-sm-8">

        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">ភាសាខ្មែរ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">ភាសាអង់គ្លេស</a>
                    </li>
                </ul>
                <div class="tab-content pl-3 p-1" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="form-group">
                            <label>ឈ្មោះក្រុមដីជាភាសាខ្មែរ</label>
                            <input class="au-input au-input--full" type="text" name="CRUD[title_kh]" data-bind-title_kh id="title_kh" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" required>
                        </div>
                        <div class="form-group">
                            <label>បរិយាយជាភាសាខ្មែរ</label>
                            <textarea class="au-input au-input--full" name="CRUD[description_kh]" data-bind-description_kh id="description_kh" data-form-textarea="true" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" ></textarea>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="form-group">
                            <label>ឈ្មោះក្រុមដីជាភាសាខ្មែរ</label>
                            <input class="au-input au-input--full" type="text" name="CRUD[title_en]" id="title_en"  data-bind-title_kh id="title_en" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" required>
                        </div>
                        <div class="form-group">
                            <label>បរិយាយជាភាសាខ្មែរ</label>
                            <textarea class="au-input au-input--full" name="CRUD[description_en]" id="description_en" data-bind-title_kh data-form-textarea="true" id="description_en"  placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" ></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

        <div class="form-group">
            <label>ភាគរយនៃផ្ទៃដៃ</label>
            <input class="au-input au-input--full" type="number"  min="1" max="100"  data-bind-percentage name="CRUD[percentage]" placeholder="ភាគរយនៃផ្ទៃដៃ" required>
        </div>
        <div class="form-group">
            <label>ជាអនុក្រុមរបស់</label>
            <select class="au-input au-input--full" id="parent_id" name="CRUD[parent_id]">
                <option value="">អត់មាន</option>
            </select>
        </div>
    </div>
    
    <div class="col-sm-4 center">
        <div class="form-group">
            <label>រូបតំណាង 414x220</label>
            <label>
                <img id="img_thumbnail" src="https://lh3.googleusercontent.com/proxy/Jnch2pmw4rWdhVbg9W1hqkL27llZTBbGNEeUD03g1NJ7PCVAecDTGX4huezF4yol8pZV3Qab4Crxqbfp1fyB0ckvUyvmHDYsYrB19J3GBqwLcBQy5QqXw1fGoXo" alt="your image" class="img-responsive">
                <input type="file" class="au-input " style="display: none" name="CRUD[thumbnail]" id="thumbnail"  data-bind-to="img_thumbnail" data-bind-thumbnail  data-form-image="true">
            </label>
        </div>
    </div>
</div>

@push('JS')
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        window.addEventListener('load', function() {
            document.getElementById('thumbnail').addEventListener('change', function() {
                if (this.files && this.files[0]) {
                    var img = document.getElementById('img_thumbnail');  // $('img')[0]
                    img.src = URL.createObjectURL(this.files[0]); // set src to blob url
                }
            });
        });
        tinymce.init({selector: '#description_kh',
            
            menubar: 'edit insert view format table',
            plugins: [
                "advlist autolink lists link image imagetools charmap preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media save table contextmenu directionality",
                "paste textcolor colorpicker textpattern"
            ],
            
            toolbar1: "image",
            file_picker_types: 'image',
            images_upload_handler: function (blobInfo, success, failure) {
                let data = new FormData();
                data.append('file', blobInfo.blob(), blobInfo.filename());
                axios.post('https://api.cardi.tech/upload', data)
                    .then(function (res) {
                        success(res.data.location);
                    })
                    .catch(function (err) {
                        failure('HTTP Error: ' + err.message);
                    });
                },

            statusbar: false, setup: function (editor) { editor.on('change', function () {tinymce.triggerSave(); }); }
        
        });

        
        tinymce.init({selector: '#description_en', 
            menubar: 'edit insert view format table',
            plugins: [
                "advlist autolink lists link image imagetools charmap preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media save table contextmenu directionality",
                "paste textcolor colorpicker textpattern"
            ],
            
            toolbar1: "image",
            file_picker_types: 'image',
            images_upload_handler: function (blobInfo, success, failure) {
                let data = new FormData();
                data.append('file', blobInfo.blob(), blobInfo.filename());
                axios.post('https://api.cardi.tech/upload', data)
                    .then(function (res) {
                        success(res.data.location);
                    })
                    .catch(function (err) {
                        failure('HTTP Error: ' + err.message);
                    });
                },

            statusbar: false, setup: function (editor) { editor.on('change', function () {tinymce.triggerSave(); }); }});

        

    </script>
    @include('component.land_parent')
@endpush