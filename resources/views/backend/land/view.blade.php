@extends('layouts.cooladmin.backend.master')
@push('CSS')
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
@endpush
@section('Content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <div class="row">
                    <div class="col-md-6">
                        <h3 class="title-5 m-b-35">បង្ហាញក្រុមដី</h3>
                    </div>
                    <div class="col-md-6 text-fight">

                    </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                
                <div class="form-group">
                    <label>ឈ្មោះក្រុមដីជាភាសាខ្មែរ</label>
                    <input class="au-input au-input--full" type="text" name="CRUD[title_kh]" data-bind-title_kh id="title_kh" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" required>
                </div>
                <div class="form-group">
                    <label>បរិយាយជាភាសាខ្មែរ</label>
                    <textarea class="au-input au-input--full" name="CRUD[description_kh]" data-bind-description_kh id="description_kh" data-form-textarea="true" placeholder="ឈ្មោះក្រុមដីជាភាសាខ្មែរ" ></textarea>
                </div>
                
            </div>
        </div>
    </div>
@endsection

@push('JS')
    @include('macro.retreive',['moduler'=>'land','id'=>$id, 'EDITOR'=>true])
@endpush