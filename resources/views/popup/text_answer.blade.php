<div class="modal fade" id="dialog-text_answer" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title fontkh">បន្ថែម</span></h4> 
        </div>
        <div class="modal-body text-center">
          <form id="formz2"  style="text-align:left;" action-api-post="{{ env('API_URL').'question-updatesub-text' }}" >
            <input  type="hidden" name="_method" id="_method" value="PUT">
            <input type="hidden" name="id" id="id_text">
            <div class="answer-block">
              <div class="form-group">
                <label>ចម្លើយជាភាសាខ្មែរ</label>
                <textarea class="au-input au-input--full"  type="text" name="title_kh" id="title_kh_text"></textarea>
              </div>
              <div class="form-group">
                <select class="au-input au-select--full"  class="link" name="link" id="ddl-land-text">
                    <label>ភ្ជាប់ក្រុមដី</label>
                    <option value="0">មិនជ្រើសរើស</option>
                </select>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" onclick="frmz2(this)"><i class="fa fa-plus"></i> រក្សាទុក</button>
        </div>
      </div>
    </div>
</div>

@push("JS")

  <script>
    function frmz2(){
      $(".waiting").show();
        var token = window.sessionStorage.getItem('access_token');
        var getUrl2 =   $('#formz2').attr("action-api-post");
        var getData = $("#formz2").serialize();
        getUrl2 += "?token=" + token;
        //$getUrl2 = getUrl2.replace("__1__", $("#student_id").val()); 
        $.ajax({
              type: 'POST',
              url: getUrl2,
              data: getData,
              headers: {
                        'Authorization': "bearer " +  window.sessionStorage.getItem('refresh_token'),
                        'Access_Token': window.sessionStorage.getItem('access_token')
                      },
            success: function(resultData) { 
                  //$(".waiting").hide();
                  window.location.reload();
            },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                  alert("Problem");
                  //$(".waiting").hide();
              }
        });
    }

 
  </script>
@endpush