<div class="modal fade" id="dialog-answer" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title fontkh">បន្ថែម</span></h4> 
        </div>
        <div class="modal-body text-center">
          <form id="formz"  style="text-align:left;" action-api-post="{{ env('API_URL').'question-updatesub' }}" >
            <input  type="hidden" name="_method" id="_method" value="PUT">
            <input type="hidden" name="id" id="student_id">
            <div class="form-group">
              <label>លេខកូដមេ</label>
              <input class="au-input au-select--full" type="text" name="parent_id" id="parent_id" readonly>
            </div>
            <input type="hidden" id="type" name="type">
            <div class="form-group">
              <label>ជ្រើសរើសប្រភេទចម្លើយ</label>
              <select class="au-input au-input--full ddlAnswer" name="answer_type_id">
                  <option value='1'>ចម្លើយ</option>
                  <option value='2'>បាទ</option>
                  <option value='3'>ទេ</option>
              </select>
            </div>
            <div class="answer-block">
              <div class="form-group">
                <label>ចម្លើយជាភាសាខ្មែរ</label>
                <textarea class="au-input au-input--full"  type="text" name="title_kh" id="title_kh"></textarea>
              </div>
              <div class="form-group">
                <label>ភ្ជាប់ក្រុមដី</label>
                <select class="au-input au-select--full"  class="link" name="link" id="ddl-land">
                  <option value="0">មិនជ្រើសរើស</option>
                </select>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" onclick="frmz(this)"><i class="fa fa-plus"></i> រក្សាទុក</button>
        </div>
      </div>
    </div>
</div>

@push("JS")

  <script>
    function frmz(){
      $(".waiting").show();
        var token = window.sessionStorage.getItem('access_token');
        var getUrl2 =   $('#formz').attr("action-api-post");
        var getData = $("#formz").serialize();
        getUrl2 += "?token=" + token;
        //$getUrl2 = getUrl2.replace("__1__", $("#student_id").val()); 
        $.ajax({
              type: 'POST',
              url: getUrl2,
              data: getData,
              headers: {
                        'Authorization': "bearer " +  window.sessionStorage.getItem('refresh_token'),
                        'Access_Token': window.sessionStorage.getItem('access_token')
                      },
            success: function(resultData) { 
                  //$(".waiting").hide();
                  window.location.reload();
            },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                  alert("Problem");
                  //$(".waiting").hide();
              }
        });
    }

 
  </script>
@endpush