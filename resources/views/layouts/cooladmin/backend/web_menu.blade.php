<ul class="list-unstyled navbar__list">
    <li class="has-sub">
        <a class="js-arrow"  href="{{ route('Backend.Dashboard.Index') }}"><i class="fas fa-tachometer-alt"></i>ផ្ទាំងតាប្លូ</a>
    </li>
   
    <li class="has-sub">
        <a class="js-arrow"  href="{{ route('Backend.Land.Index') }}"><i class="fas fa-tachometer-alt"></i>ក្រុមដី</a>
    </li>
    <li class="has-sub">
        <a class="js-arrow" href="{{ route('Backend.Question.Index') }}"><i class="fas fa-tachometer-alt"></i>សំនួរ</a>
    </li>

        
    <li class="has-sub">
        <a class="js-arrow" href="{{ route('Backend.Rice.Index') }}"><i class="fas fa-tachometer-alt"></i>ពូជដំណាំស្រូវ</a>
    </li>

    <li class="has-sub"  style="display:none">
        <a class="js-arrow" href="{{ route('Backend.Fertilizer.Index') }}"><i class="fas fa-tachometer-alt"></i>ប្រភេទជី</a>
    </li>
    
    <li class="has-sub">
        <a class="js-arrow" href="{{ route('Backend.User.Index') }}"><i class="fas fa-tachometer-alt"></i>អ្នកប្រើប្រាស់</a>
    </li>


   
</ul>