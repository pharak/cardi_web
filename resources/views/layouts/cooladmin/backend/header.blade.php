<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/css/font-face.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/css/theme.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/jqueryconfirm/css/jquery-confirm.css" rel="stylesheet" media="all">
    <style>
        .left{text-align: left}
        .right{text-align: right}
        .account-wrap{position: inherit !important}
    </style>
    @stack('CSS')
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <img src="{{ url('CARDI_logo.JPG') }}" alt="CARDI" width="60" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                @include('layouts.cooladmin.backend.mobile_menu')
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <h2>CARDI Admin</h2>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <center>
                    <img src="{{ url('CARDI_logo.JPG') }}" alt="CARDI" width="280" />
                </center>
                <nav class="navbar-sidebar">
                    @include('layouts.cooladmin.backend.web_menu')
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                        
                            </form>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                    
                                        <div class="content">
                                            <a class="username_account js-acc-btn" href="#" >john doe</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="{{ url('/') }}/vendors/cooladmin/images/icon/avatar-01.jpg" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#" class="fullname_account">john doe</a>
                                                    </h5>
                                                    <span class="email_account">johndoe@example.com</span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="#" id="btnLogout">
                                                    <i class="zmdi zmdi-power" ></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">