                </div>
            </div>
        </div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
    </div>

</div>

<!-- Jquery JS-->
<script src="{{ url('/') }}/vendors/cooladmin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="{{ url('/') }}/vendors/cooladmin/vendor/slick/slick.min.js">
</script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/wow/wow.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/animsition/animsition.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/circle-progress/circle-progress.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="{{ url('/') }}/vendors/cooladmin/js/main.js"></script>
<script src="{{ url('/') }}/vendors/jqueryconfirm/js/jquery-confirm.js"></script>
<script src="{{ url('/') }}/vendors/tinymce/js/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    $.ajaxSetup({
        beforeSend: function (xhr)
        {
            $getAuthorization = window.sessionStorage.getItem('token');
            xhr.setRequestHeader("Authorization","bearer " + $getAuthorization);      
        }
    });
    $(document).ready(function(){
        $.ajaxSetup({
            beforeSend: function (xhr)
            {
                $getAuthorization = window.sessionStorage.getItem('token');
                xhr.setRequestHeader("Authorization","bearer " + $getAuthorization);      
            }
        });
    })
      
    $("#btnLogout").click(function(){
        $.ajax({
            type: 'GET',
            url: "{{ env('API_URL').'logout' }}",
            success: function(resultData) { 
                window.sessionStorage.clear();
                window.location.href = "{{ url('/') }}";
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                window.sessionStorage.clear();
                window.location.href = "{{ url('/') }}";                
            }  
        });
    });
    if(!window.sessionStorage.getItem('token')){
       // window.location.href = "{{ url('/') }}";
    }else{
        $('.username_account').html(window.sessionStorage.getItem('username'));
        $('.email_account').html(window.sessionStorage.getItem('email'));
        $('.fullname_account').html(window.sessionStorage.getItem('fullname'));
    }
</script>
@stack('JS')
</body>

</html>
<!-- end document-->
