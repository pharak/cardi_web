<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login</title>

    <!-- Fontfaces CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/css/font-face.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="{{ url('/') }}/vendors/cooladmin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ url('/') }}/vendors/cooladmin/css/theme.css" rel="stylesheet" media="all">
    @stack('CSS')
</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">