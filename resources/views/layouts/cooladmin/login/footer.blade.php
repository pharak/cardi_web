                </div>
            </div>
        </div>
    </div>

</div>

<!-- Jquery JS-->
<script src="{{ url('/') }}/vendors/cooladmin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="{{ url('/') }}/vendors/cooladmin/vendor/slick/slick.min.js">
</script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/wow/wow.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/animsition/animsition.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/circle-progress/circle-progress.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="{{ url('/') }}/vendors/cooladmin/vendor/select2/select2.min.js">

</script>
@stack('JS')

<!-- Main JS-->
    <script src="{{ url('/') }}/vendors/cooladmin/js/main.js"></script>
 
</body>

</html>
<!-- end document-->