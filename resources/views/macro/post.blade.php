<script>
     let saveAjaxData;
     $('#btn-post').confirm({
        title: 'ប្រកាស',
        content: 'តើអ្នកពិតជាចង់បង្កើតធាតុចូលថ្មីនេះ?',
        buttons: {
            យល់ព្រម: function () {
                post();
            },
            បដិសេធ: function () {
            }
        }
    });
    
    $("#btn-cancel").click(function(){
        window.location.href = $('#myform').attr("action-main-url");
    });
    function post(){
        let getData = $('#myform').serialize();
        $(".waiting").show();
        saveAjaxData = $.ajax({
            type: 'POST',
            url:  $('#myform').attr("action-api-post"),
            data: getData,
            success: function(resultData) { 
                $(".waiting").hide();
            },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alert("Problem");
                $(".waiting").hide();
            }
        }).done(function( msg ) {
            $(".hide").show();
            $.alert({
                title: 'ប្រកាស',
                content: 'ធាតុចូលរក្សារទុក',
                buttons: {
                    confirm: {
                        text: 'បានដឹង',
                        btnClass: 'btn-blue',
                        action: function () {
                            window.location.href =  $('#myform').attr("action-main-url");
                        }
                    }
                }
            });
        });
    }
</script>