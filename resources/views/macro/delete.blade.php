<script>
    $('#btn-delete').confirm({
        title: 'ប្រកាស',
        content: 'តើអ្នកពិតជាចង់លុបធាតុចូលមួយនេះ?',
        buttons: {
            យល់ព្រម: function () {
                destroy();
            },
            បដិសេធ: function () {
            }
        }
    });
    function destroy(){
        $(".waiting").show();
            $.ajax({
                type: 'DELETE',
                url:  $('#form').attr("action-api-delete"),
                headers: { 'Authorization': "bearer " +  window.localStorage.getItem('refresh_token'),
                        'Access_Token': window.localStorage.getItem('access_token')
                },
                success: function(resultData) { 
                    $(".waiting").hide();
                },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    alert("Problem");
                    $(".waiting").hide();
                }
            }).done(function( msg ) {
                $.alert({
                    title: 'ប្រកាស',
                    content: 'លុបធាតុចូលជោគជ័យ',
                    buttons: {
                        confirm: {
                            text: 'បានដឹង',
                            btnClass: 'btn-blue',
                            action: function () {
                                window.location.href =  $('#form').attr("action-main-url");
                            }
                        }
                    }
                });
            });    
        }
</script>