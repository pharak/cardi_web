<script>
    let getAjaxData;
    function getDataForm(){
        $(".waiting").show();
        let getUrl = "{{ env('API_URL').$moduler.'/'.$id }}"; 
        getAjaxData = $.ajax({
            type: 'GET',
            url:  getUrl,
            success: function(resultData) { 
                $(".waiting").hide();
                $.each(resultData, function(index, value) {                        
                    if($(".au-input[data-bind-"+index+"]").attr('type') == "text")
                    {
                        $(".au-input[data-bind-"+index+"]").val(value);
                    }
                    else if($(".au-input[data-bind-"+index+"]").attr('type') == "radio" || $(".au-input[data-bind-"+index+"]").attr('type') == "checkbox")
                    {
                        $(".au-input[data-bind-"+index+"]").prop("checked", true);
                        $(".au-input[data-bind-"+index+"]").attr("checked", "checked");
                    }
                    else if($(".au-input[data-bind-"+index+"]").attr('data-select') == "true")
                    {
                        $(".au-input[data-bind-"+index+"]").val(value);
                    }
                    else if($(".au-input[data-bind-"+index+"]").attr('data-form-image') == "true")
                    {
                        if(value){
                            $("#img_" + index).attr("src",value);
                        }
                    }else if($(".au-input[data-bind-"+index+"]").attr('data-form-textarea') == "true"){
                        $("#"+index).text(value);
                        $("#"+index).val(value);
                        @if(@$EDITOR)
                            tinyMCE.get(index).setContent(value);
                        @endif
                    }else if($(".au-input[data-bind-"+index+"]").attr('type') == "file"){
                        
                    }
                    else{
                        $(".au-input[data-bind-"+index+"]").val(value);
                    }

                    @if(@$Math)
                        $(".mathpreview-"+index).html(value);
                        MathJax.texReset();
                        MathJax.typesetClear();
                        MathJax.typeset();
                    @endif

                });

            },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alert('Problem')
                $(".waiting").hide();        
                getDataForm();
            }
        });            
    }
    $(document).ready(function(){
        getDataForm();
    })
</script>