<script>
    $(document).ready(function(){
        $(".myform").submit(function(e){
            e.preventDefault();
            var myform = $(this);
            $.confirm({
                title: 'ប្រកាស',
                content: 'តើអ្នកពិតជាចង់បង្កើតធាតុចូលថ្មីនេះ?',
                buttons: {
                    យល់ព្រម: function () {
                        var getData = new FormData(myform[0]);
                        saveAjaxData = $.ajax({
                                type: 'POST',
                                url:  myform.attr("action-api-post"),
                                data: getData,
                                dataType: "html",
                                contentType: false,
                                processData: false,
                            success: function(resultData) { 
                                    $(".waiting").hide();
                            },error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                    alert("Problem");
                                    $(".waiting").hide();
                                }
                        }).done(function( msg ) {
                                $(".waiting").hide();
                            $.alert({
                                title: 'ប្រកាស',
                                content: 'ធាតុចូលរក្សារទុក',
                                buttons: {
                                    confirm: {
                                        text: 'បានដឹង',
                                        btnClass: 'btn-blue',
                                        action: function () {
                                            window.location.href =  myform.attr("action-main-url");
                                        }
                                    }
                                }
                            });
                        });
                    },
                    បដិសេធ: function () {
                    }
                }
            });
           
            $("#btn-cancel").click(function(){
                window.location.href = $('.myfrom').attr("action-main-url");
            });
        });
    });
   
</script>