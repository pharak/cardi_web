<?php

namespace App\Http\Controllers;

class LandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function index(){
        return view('backend.land.index');
    }
    public function create(){
        return view('backend.land.create');
    }
    public function edit($id){
        return view('backend.land.edit',compact('id'));
    }
    public function view($id){
        return view('backend.land.view',compact('id'));
    }
    public function photo($id){
        return view('backend.land.photo',compact('id'));
    }
    //
}
