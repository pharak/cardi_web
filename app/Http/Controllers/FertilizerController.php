<?php

namespace App\Http\Controllers;

class FertilizerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        return view('backend.fertilizer.index');
    }

    public function create(){
        return view('backend.fertilizer.create');
    }
    public function edit($id){
        return view('backend.fertilizer.edit',compact('id'));
    }
    //
}
