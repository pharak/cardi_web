<?php

namespace App\Http\Controllers;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        return view('backend.user.index');
    }

    public function create(){
        return view('backend.user.create');
    }

    public function edit($id){
        return view('backend.user.edit',compact('id'));
    }

    //
}
