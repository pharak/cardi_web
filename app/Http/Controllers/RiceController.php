<?php

namespace App\Http\Controllers;

class RiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        return view('backend.rice.index');
    }

    public function create(){
        return view('backend.rice.create');
    }
    public function edit($id){
        return view('backend.rice.edit',compact('id'));
    }
    //
}
