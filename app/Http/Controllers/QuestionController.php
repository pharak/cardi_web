<?php

namespace App\Http\Controllers;

class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        return view('backend.question.index');
    }

    public function create(){
        return view('backend.question.create');
    }
    public function edit($id){
        return view('backend.question.edit',compact('id'));
    }
    public function stucture($id){
        return view('backend.question.edit',compact('id'));
    }
    public function view($id){
        return view('backend.question.struture',compact('id'));
    }

    //
}
